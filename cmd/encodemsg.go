package main

import (
	"fmt"
	"os"

	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/nats-io/nats.go"
	"gitlab.com/chanshare/manager/message"
)

var roomName string

type screen int

const (
	MsgSel = iota
	MsgParam
	Done
)

var docStyle = lipgloss.NewStyle().Margin(1, 2)

var Mappings = map[string]int{
	"Play":  message.Play,
	"Pause": message.Pause,
	"Seek":  message.Seek,
	"Join":  message.Join,
	"Leave": message.Leave,
	"Next":  message.Next,
	"Prev":  message.Prev,
	"Media": message.Media,
	"Kill":  message.Kill,
}

type model struct {
	list     list.Model
	selected string
	param    textinput.Model
	screen   int
	err      error
}

type item struct {
	title, desc string
}

func (i item) Title() string       { return i.title }
func (i item) Description() string { return i.desc }
func (i item) FilterValue() string { return i.title }

var List = []list.Item{
	item{title: "Play", desc: "All clients begin playback"},
	item{title: "Pause", desc: "All clients pause playback"},
	item{title: "Seek", desc: "All clients seek to set time"},
	item{title: "Join", desc: "A new client has joined"},
	item{title: "Leave", desc: "A client has left"},
	item{title: "Next", desc: "Skip to next media"},
	item{title: "Prev", desc: "Skip to previous media"},
	item{title: "Media", desc: "Load this media"},
	item{title: "Kill", desc: "Kill the room"},
}

func (m model) Init() tea.Cmd {
	return nil
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c":
			return m, tea.Quit
		case "enter":
			switch m.screen {
			case MsgSel:
				i, ok := m.list.SelectedItem().(item)
				if !ok {
					panic("Not a string")
				}
				m.selected = i.title
				m.screen = MsgParam
			case MsgParam:
				msgS := message.Message{
					Action: byte(Mappings[m.selected]),
					Param:  m.param.Value(),
				}
				err := sendMessage(msgS)
				if err != nil {
					m.err = err
				}
				m.screen = Done
			}
		}
	case tea.WindowSizeMsg:
		h, v := docStyle.GetFrameSize()
		m.list.SetSize(msg.Width-h, msg.Height-v)
	}

	var cmd tea.Cmd
	if m.screen == MsgSel {
		m.list, cmd = m.list.Update(msg)
	} else if m.screen == MsgParam {
		m.param, cmd = m.param.Update(msg)
	}
	return m, cmd
}

func (m model) View() string {
	if m.err != nil {
		return m.err.Error()
	}
	switch m.screen {
	case MsgSel:
		return docStyle.Render(m.list.View())
	case MsgParam:
		return docStyle.Render(
			fmt.Sprintf(
				"Whats the command param?\n\n%s\n\n%s",
				m.param.View(),
				"(ctrl+c to quit)",
			) + "\n",
		)
	case Done:
		return docStyle.Render("Message sent successfully\n\nPress ctrl+c to exit")
	default:
		return docStyle.Render("Something went wrong")
	}
}

func sendMessage(msg message.Message) error {
	bytes := msg.Encode()

	conn, err := nats.Connect("localhost:4222")
	if err != nil {
		return err
	}

	js, err := conn.JetStream()
	if err != nil {
		return err
	}

	m := nats.NewMsg(roomName + ".*")
	m.Header.Add("chanshare", "true")
	m.Data = append(m.Data, bytes...)
	_, err = js.PublishMsg(m)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	for i, a := range os.Args {
		if a == "-r" {
			roomName = os.Args[i+1]
		} else {
			roomName = "illegal-swallow-road"
		}
	}

	ti := textinput.New()
	ti.Placeholder = "test"
	ti.Focus()
	ti.CharLimit = 80
	m := model{
		list:     list.New(List, list.NewDefaultDelegate(), 0, 0),
		selected: "",
		param:    ti,
		screen:   MsgSel,
	}
	m.list.Title = "Message Type"
	p := tea.NewProgram(m, tea.WithAltScreen())
	if err := p.Start(); err != nil {
		fmt.Printf("Error: %v, roomName: %v", err, roomName)
		os.Exit(1)
	}
}
