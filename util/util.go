package util

import (
	"context"
	"errors"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"go.opentelemetry.io/otel"
)

const Manager = "manager"

type CtxKey string

var errNoLogger = errors.New("missing logger")
var errNotTypeLogger = errors.New("context value is not of type *otelzap.Logger")
var errNotTypePostgres = errors.New("context value is not of type *gorm.DB")

const (
	Logger CtxKey = "logger"
)

func LoggerFromContext(ctx context.Context) (*otelzap.Logger, error) {
	ctx, span := otel.Tracer(Manager).Start(ctx, "LoggerFromContext")
	defer span.End()
	loggerInf := ctx.Value(Logger)
	logger, ok := loggerInf.(*otelzap.Logger)
	if !ok {
		return nil, errNotTypeLogger
	}

	return logger, nil
}
