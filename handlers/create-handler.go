package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/valyala/fasthttp"
	"gitlab.com/chanshare/manager/model"
	"gitlab.com/chanshare/manager/util"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

func (s *Server) CreateRoomHandler(rw http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	span := trace.SpanFromContext(ctx)
	defer span.End()
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		writeError(rw, err, http.StatusInternalServerError)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	var request model.CreateRoomRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		logger.Ctx(ctx).Error("Failed to decode request", zap.Error(err))
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	if err := request.Validate(); err != nil {
		logger.Ctx(ctx).Error("Failed to validate request", zap.Error(err))
		writeError(rw, err, fasthttp.StatusBadRequest)
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return
	}

	room, signal := s.RoomManager.NewRoom(request.Name, request.User, request.Playlist)

	// TODO: Will need some sort of overhead to manage the lifecycle of a room from a goroutine perspective
	logger.Info("Starting room", zap.String("RoomName", request.Name))
	go room.Run(ctx)

	for val := range signal {
		if val == false {
			rw.WriteHeader(fasthttp.StatusInternalServerError)
			return
		}
	}

	// Tell the calling service that the room has been created
	logger.Info("Writing success header")
	rw.WriteHeader(fasthttp.StatusCreated)
}
