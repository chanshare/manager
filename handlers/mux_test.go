package handlers

import (
	"context"
	"testing"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/manager/config"
	"go.opentelemetry.io/otel/sdk/trace"
	"go.uber.org/zap"
)

// TODO: This is not a very good test
func TestNewServer(t *testing.T) {
	type args struct {
		ctx  context.Context
		conf config.Config
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "HappyPath",
			args: args{
				ctx:  genCtx(t),
				conf: config.InitConfig(genCtx(t), "../.conf"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// TODO!
			tp := trace.NewTracerProvider()
			exp := tp.Tracer("test")

			_, err := NewServer(tt.args.conf, exp, otelzap.New(zap.NewExample()), "testUrl")
			if err != nil {
				t.FailNow()
			}
		})
	}
}
