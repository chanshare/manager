package handlers

import (
	"context"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chanshare/manager/config"
	roomMock "gitlab.com/chanshare/manager/room/mocks"
	"gitlab.com/chanshare/manager/util"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.uber.org/zap"
)

func Test_CreateRoomHandler(t *testing.T) {
	var comm chan bool
	tests := []struct {
		Name         string
		Ctx          context.Context
		Req          string
		StatusCode   int
		ShouldClose  bool
		Expectations func(*roomMock.RoomManager, *roomMock.Room)
	}{
		{
			Name:        "Happy path",
			Ctx:         genCtx(t),
			Req:         happyPathJson,
			StatusCode:  http.StatusCreated,
			ShouldClose: true,
			Expectations: func(manager *roomMock.RoomManager, room *roomMock.Room) {
				comm = make(chan bool)
				manager.On("NewRoom", "testroom", "testuser", []string{"testmedia"}).Return(room, comm)
				room.On("Run", mock.Anything).Return(nil)
			},
		},
		{
			Name:       "Unhappy path, invalid request",
			Req:        unhappyPathJson,
			Ctx:        genCtx(t),
			StatusCode: http.StatusBadRequest,
			Expectations: func(manager *roomMock.RoomManager, room *roomMock.Room) {
			},
		},
		{
			Name:        "Unhappy path, room.Run fails",
			Req:         happyPathJson,
			Ctx:         genCtx(t),
			StatusCode:  http.StatusInternalServerError,
			ShouldClose: false,
			Expectations: func(manager *roomMock.RoomManager, room *roomMock.Room) {
				comm = make(chan bool, 1)
				comm <- false
				manager.On("NewRoom", "testroom", "testuser", []string{"testmedia"}).Return(room, comm)
				room.On("Run", mock.Anything).Return(nil)
			},
		},
		{
			Name:        "Unhappy path, no logger in context",
			Req:         happyPathJson,
			Ctx:         context.Background(),
			StatusCode:  http.StatusInternalServerError,
			ShouldClose: false,
			Expectations: func(manager *roomMock.RoomManager, room *roomMock.Room) {
			},
		},
		{
			Name:        "Unhappy path, invalid json",
			Req:         garbledReq,
			Ctx:         genCtx(t),
			StatusCode:  http.StatusBadRequest,
			ShouldClose: false,
			Expectations: func(manager *roomMock.RoomManager, room *roomMock.Room) {
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			managerMock := roomMock.NewRoomManager(t)
			mockRoom := roomMock.NewRoom(t)
			roomMock.NewRoom(t)
			tt.Expectations(managerMock, mockRoom)

			if tt.ShouldClose {
				close(comm)
			}

			server := Server{
				RoomManager: managerMock,
				Config:      config.InitConfig(genCtx(t), "../.conf"),
			}

			req := httptest.NewRequest("POST", "/room", strings.NewReader(tt.Req))
			w := httptest.NewRecorder()
			req = req.WithContext(tt.Ctx)

			mid := otelmux.Middleware("test")

			handler := mid(http.HandlerFunc(server.CreateRoomHandler))

			handler.ServeHTTP(w, req)

			time.Sleep(time.Second * 1)
			assert.Equal(t, tt.StatusCode, w.Result().StatusCode)
		})
	}
}

func genCtx(t *testing.T) context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, util.Logger, otelzap.New(zap.NewExample()))
	return ctx
}

const happyPathJson = `
{
  "user": "testuser",
  "playlist": [
    "testmedia"
  ],
  "name": "testroom"
}	`

const unhappyPathJson = `
	{
"user": "tester",
"board_sn": "string"
}
	`

const garbledReq = `
	ksldjfghlshdvkajshbldbhl
	dkfjghallksjghruihakljvh
	laskurghuaibyughalzuahbl
	`
