package handlers

import (
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/manager/config"
	"gitlab.com/chanshare/manager/room"
	"gitlab.com/chanshare/manager/util"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"
	"go.opentelemetry.io/otel/trace"

	"github.com/gorilla/mux"
)

const (
	Room = "/room"
)

type Server struct {
	Router      *mux.Router
	Tracer      trace.Tracer
	Config      config.Config
	RoomManager room.RoomManager
}

func NewServer(conf config.Config, tracer trace.Tracer, logger *otelzap.Logger, natsURL string) (Server, error) {
	r := mux.NewRouter()
	s := Server{
		Router:      r,
		Tracer:      tracer,
		Config:      conf,
		RoomManager: room.NewManager(natsURL),
	}
	r.Use(middleware(logger))
	r.Use(otelmux.Middleware(util.Manager))
	r.HandleFunc(Room, s.CreateRoomHandler).Methods("POST")
	return s, nil
}
