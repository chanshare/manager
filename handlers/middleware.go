package handlers

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/manager/util"
)

func middleware(logger *otelzap.Logger) mux.MiddlewareFunc {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			ctx = context.WithValue(ctx, util.Logger, logger)
			r = r.WithContext(ctx)
			h.ServeHTTP(w, r)
		})
	}
}
