package handlers

import (
	"context"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/manager/util"
	"go.uber.org/zap"
)

func Test_middleware(t *testing.T) {
	type args struct {
		logger *otelzap.Logger
	}
	tests := []struct {
		name string
		args args
		want context.Context
	}{
		{
			name: "HappyPath",
			args: args{
				logger: otelzap.New(zap.NewExample()),
			},
			want: genCtx(t),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := middleware(tt.args.logger)

			handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				ctx := r.Context()
				if reflect.TypeOf(ctx.Value(util.Logger)).String() != "*otelzap.Logger" {
					t.FailNow()
				}
			})
			req := httptest.NewRequest("GET", "http://testing", nil)

			testHandler := got(handler)

			// call the handler using a mock response recorder (we'll not use that anyway)
			testHandler.ServeHTTP(httptest.NewRecorder(), req)
		})
	}
}
