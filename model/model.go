package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type CreateRoomRequest struct {
	User     string   `json:"user"`
	Playlist []string `json:"playlist"`
	Name     string   `json:"name"`
}

func (r CreateRoomRequest) Validate() error {
	return validation.ValidateStruct(&r,
		validation.Field(&r.User, validation.Required, is.Alphanumeric),
		validation.Field(&r.Playlist, validation.Required),
		validation.Field(&r.Name, validation.Required, is.PrintableASCII),
	)
}
