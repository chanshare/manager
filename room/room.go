package room

import (
	"context"
	"sync"

	"github.com/nats-io/nats.go"
	"gitlab.com/chanshare/manager/message"
	"gitlab.com/chanshare/manager/util"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.uber.org/zap"
)

type RoomStruct struct {
	Mutex    sync.Mutex
	RoomName string
	Playlist []string
	Owner    string
	Users    []string
	NatsURL  string
	Index    int
	Subject  string
	Comm     chan bool
}

//go:generate mockery --name Room --disable-version-string
type Room interface {
	Run(context.Context)
}

// Opens the NATS 'topic' and handle the flow of event
func (r *RoomStruct) Run(ctx context.Context) {
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		r.Comm <- false
		return
	}
	nc, err := nats.Connect(r.NatsURL)
	if err != nil {
		logger.Error("Failed to connect to NATS", zap.Error(err))
		r.Comm <- false
		return
	}

	js, err := nc.JetStream()
	if err != nil {
		logger.Error("Failed to jetstreamify", zap.Error(err))
		r.Comm <- false
		return
	}

	logger.Debug("connected to ", zap.String("url", r.NatsURL))

	js.AddStream(&nats.StreamConfig{
		Name:     r.RoomName,
		Subjects: []string{r.Subject},
	})

	com := make(chan message.Message)

	var wg sync.WaitGroup
	js.Subscribe(r.Subject, r.handleMessage(ctx, com), nats.DeliverNew())
	wg.Add(1)
	go r.sendMessage(ctx, com, js)

	go func() {
		m := <-com
		if m.Action == message.Kill {
			wg.Done()
		}
	}()

	r.Comm <- true
	close(r.Comm)

	wg.Wait()

}

func (r *RoomStruct) leave(user string) {
	newUserList := make([]string, 0)
	r.Mutex.Lock()
	for _, rUser := range r.Users {
		if rUser != user {
			newUserList = append(newUserList, rUser)
		}
	}
	r.Users = newUserList
	r.Mutex.Unlock()
}

func (r *RoomStruct) handleMessage(ctx context.Context, com chan message.Message) func(*nats.Msg) {
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		return nil
	}
	return func(m *nats.Msg) {
		tr := otel.Tracer("room-" + r.RoomName)
		_, span := tr.Start(ctx, "handleMessage")
		defer span.End()
		header := m.Header.Get("chanshare")
		if header == "true" {
			logger.Debug("Processing message", zap.Any("msg", m))
			msg, err := message.Decode(m.Data)
			if err != nil {
				logger.Error("Error decoding message", zap.Error(err))
				return
			}
			logger.Info("Message received",
				zap.String("action", message.Mappings[int(msg.Action)]),
				zap.Any("param", msg.Param))

			switch msg.Action {
			case message.Join:
				logger.Info("Actioning join")
				span.SetAttributes(
					attribute.Key("messageType").String("join"),
					attribute.Key("user").String(msg.Param.(string)))

				r.Mutex.Lock()
				r.Users = append(r.Users, msg.Param.(string))
				logger.Info("Added user")
				r.Mutex.Unlock()
				com <- msg
				logger.Info("Msg sent back down channel")

			case message.Leave:
				logger.Info("Actioning leave")
				span.SetAttributes(
					attribute.Key("messageType").String("leave"),
					attribute.Key("user").String(msg.Param.(string)))
				r.leave(msg.Param.(string))
				com <- msg
			case message.Next:
				logger.Debug("Hit next message")
				span.SetAttributes(attribute.Key("messageType").String("next"))
				if r.Index == len(r.Playlist)-1 {
					logger.Debug("Wrapping index around")
					r.Index = 0
				} else {
					logger.Debug("Adding to index")
					r.Index++
				}
				logger.Debug("index", zap.Int("index", r.Index))
				com <- message.Message{
					Action: message.Media,
					Param:  r.Playlist[r.Index],
				}

			case message.Prev:
				span.SetAttributes(attribute.Key("messageType").String("prev"))
				if r.Index == 0 {
					r.Index = len(r.Playlist) - 1
				} else {
					r.Index--
				}
				com <- message.Message{
					Action: message.Media,
					Param:  r.Playlist[r.Index],
				}

			case message.Pause, message.Play, message.Seek:
				span.SetAttributes(attribute.Key("messageType").String("client side"))
				com <- msg
			default:
				span.SetStatus(codes.Error, "no matching message type")
				logger.Warn("There was no matching action branch")
			}
			logger.Debug("Summary of changes", zap.Any("users", r.Users))
		}
	}
}

/*
 */

func (r *RoomStruct) sendMessage(ctx context.Context, com chan message.Message, js nats.JetStreamContext) {
	logger, _ := util.LoggerFromContext(ctx)
	for {
		msgStruct := <-com
		msg := msgStruct.Encode()
		_, err := js.Publish(r.Subject, msg)
		if err != nil {
			logger.Error("failed to send message", zap.Error(err))
		}
	}
}
