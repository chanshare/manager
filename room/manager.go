package room

type RoomMan struct {
	NatsURL string
	rooms   []Room
}

//go:generate mockery --name RoomManager --disable-version-string
type RoomManager interface {
	NewRoom(string, string, []string) (Room, chan bool)
}

func NewManager(natsURL string) RoomManager {
	return &RoomMan{
		rooms:   make([]Room, 0),
		NatsURL: natsURL,
	}
}

func (m *RoomMan) NewRoom(name, owner string, playlist []string) (Room, chan bool) {
	signal := make(chan bool)
	room := RoomStruct{
		RoomName: name,
		Playlist: playlist,
		Owner:    owner,
		NatsURL:  m.NatsURL,
		Comm:     signal,
		Subject:  name + ".*",
	}
	m.rooms = append(m.rooms, &room)
	return &room, signal
}
