package room

import (
	"context"
	"os"
	"testing"

	"github.com/nats-io/nats-server/v2/server"
	natsserver "github.com/nats-io/nats-server/v2/test"
	"github.com/nats-io/nats.go"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/manager/message"
	"gitlab.com/chanshare/manager/util"
	"go.uber.org/zap"
)

func TestRoomRun(t *testing.T) {
	nServer := natsTestServer(12341)
	defer nServer.Shutdown()
	tests := []struct {
		Name           string
		Ctx            context.Context
		ExpectedResult bool
		ShouldKillRoom bool
		NatsURL        string
	}{
		{
			Name:           "Happy path",
			Ctx:            genCtx(t),
			ExpectedResult: true,
			ShouldKillRoom: true,
			NatsURL:        "localhost:12341",
		},
		{
			Name:           "Unhappy path, no logger",
			Ctx:            context.Background(),
			ExpectedResult: false,
			ShouldKillRoom: false,
			NatsURL:        "localhost:12341",
		},
		{
			Name:           "Unhappy path, bad nats url",
			Ctx:            genCtx(t),
			ExpectedResult: false,
			ShouldKillRoom: false,
			NatsURL:        "localhost:1290",
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			manager := NewManager(tt.NatsURL)
			room, comm := manager.NewRoom("testroom", "testuser", []string{"testmedia"})
			if tt.ShouldKillRoom {
				nc, _ := nats.Connect("localhost:12341")
				js, _ := nc.JetStream()
				js.Publish("testroom.*", message.Message{Action: message.Kill}.Encode())
			}

			go room.Run(tt.Ctx)

			result := <-comm
			if result != tt.ExpectedResult {
				t.FailNow()
			}

		})
	}
}

func TestHandleMessage(t *testing.T) {
	manager := NewManager("localhost:12341")
	room, _ := manager.NewRoom("testroom", "testuser", []string{"testmedia", "testmedia2"})

	roomS := room.(*RoomStruct)

	msgChan := make(chan message.Message)
	ctx := genCtx(t)
	handler := roomS.handleMessage(ctx, msgChan)

	tests := []struct {
		name        string
		msg         message.Message
		expectation func(room *RoomStruct, t *testing.T, m message.Message)
	}{
		{
			name: "Join",
			msg: message.Message{
				Action: message.Join,
				Param:  "secondUser",
			},
			expectation: func(room *RoomStruct, t *testing.T, m message.Message) {
				if !lo.Contains(roomS.Users, "secondUser") {
					t.FailNow()
				}
				assert.Equal(t, m.Action, byte(message.Join))
				assert.Equal(t, m.Param, "secondUser")
			},
		},
		{
			name: "Leave",
			msg: message.Message{
				Action: message.Leave,
				Param:  "secondUser",
			},
			expectation: func(room *RoomStruct, t *testing.T, m message.Message) {
				if lo.Contains(roomS.Users, "secondUser") {
					t.FailNow()
				}
				assert.Equal(t, m.Action, byte(message.Leave))
				assert.Equal(t, m.Param, "secondUser")
			},
		},
		{
			name: "Next",
			msg: message.Message{
				Action: message.Next,
			},
			expectation: func(room *RoomStruct, t *testing.T, m message.Message) {
				assert.Equal(t, roomS.Index, 1)
				assert.Equal(t, m.Action, byte(message.Media))
				assert.Equal(t, m.Param, "testmedia2")
			},
		},
		{
			name: "Prev",
			msg: message.Message{
				Action: message.Prev,
				Param:  nil,
			},
			expectation: func(room *RoomStruct, t *testing.T, m message.Message) {
				assert.Equal(t, roomS.Index, 0)
				assert.Equal(t, m.Action, byte(message.Media))
				assert.Equal(t, m.Param, "testmedia")
			},
		},
		{
			name: "Play",
			msg: message.Message{
				Action: message.Play,
				Param:  123,
			},
			expectation: func(room *RoomStruct, t *testing.T, m message.Message) {
				assert.Equal(t, m.Action, byte(message.Play))
				assert.Equal(t, int(m.Param.(uint64)), 123)
			},
		},
		{
			name: "Pause",
			msg: message.Message{
				Action: message.Pause,
				Param:  123,
			},
			expectation: func(room *RoomStruct, t *testing.T, m message.Message) {
				assert.Equal(t, m.Action, byte(message.Pause))
				assert.Equal(t, int(m.Param.(uint64)), 123)
			},
		},
		{
			name: "Seek",
			msg: message.Message{
				Action: message.Seek,
				Param:  123,
			},
			expectation: func(room *RoomStruct, t *testing.T, m message.Message) {
				assert.Equal(t, m.Action, byte(message.Seek))
				assert.Equal(t, int(m.Param.(uint64)), 123)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			natsMsg := nats.NewMsg("testroom.*")
			natsMsg.Data = tt.msg.Encode()
			natsMsg.Header.Add("chanshare", "true")
			go handler(natsMsg)

			outMsg := <-msgChan

			tt.expectation(roomS, t, outMsg)
		})
	}

}

func sendMsg(js nats.JetStreamContext, msg message.Message) error {
	m := nats.NewMsg("testroom.*")
	m.Data = msg.Encode()
	m.Header.Add("chanshare", "true")
	_, err := js.PublishMsg(m)
	return err
}

func setupJS(comm chan bool) (nats.JetStreamContext, error) {
	nc, err := nats.Connect("localhost:12341")
	if err != nil {
		comm <- false
		return nil, err
	}

	js, err := nc.JetStream()
	if err != nil {
		comm <- false
		return nil, err
	}

	_, err = js.AddStream(&nats.StreamConfig{
		Name:     "testroom",
		Subjects: []string{"testroom.*"},
	})
	return js, err
}

func genCtx(t *testing.T) context.Context {
	ctx := context.Background()
	ctx = context.WithValue(ctx, util.Logger, otelzap.New(zap.NewExample()))
	return ctx
}

func natsTestServer(port int) *server.Server {
	opts := natsserver.DefaultTestOptions
	opts.StoreDir = "/tmp/nats_tst"
	opts.JetStream = true
	opts.Port = port
	return natsserver.RunServer(&opts)
}

func cleanup() error {
	return os.RemoveAll("/tmp/nats_test")
}

func checkErr(t *testing.T, err error) {
	if err != nil {
		t.FailNow()
	}
}
