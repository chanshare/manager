package room

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewManager(t *testing.T) {
	manager := NewManager("test")

	managerInf := reflect.TypeOf((*RoomManager)(nil)).Elem()
	if !reflect.TypeOf(manager).Implements(managerInf) {
		t.FailNow()
	}

	man, ok := manager.(*RoomMan)
	if !ok {
		t.FailNow()
	}

	assert.Equal(t, "test", man.NatsURL)
}

func TestNewRoom(t *testing.T) {
	manager := NewManager("test")
	room, _ := manager.NewRoom("testroom", "testuser", []string{"testmedia"})

	roomInf := reflect.TypeOf((*Room)(nil)).Elem()
	if !reflect.TypeOf(room).Implements(roomInf) {
		t.FailNow()
	}

	rm, ok := room.(*RoomStruct)
	if !ok {
		t.FailNow()
	}

	assert.Equal(t, "test", rm.NatsURL)
	assert.Equal(t, "testroom", rm.RoomName)
	assert.Equal(t, "testuser", rm.Owner)
	assert.Equal(t, []string{"testmedia"}, rm.Playlist)
	assert.Equal(t, "testroom.*", rm.Subject)
}
