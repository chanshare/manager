package main

import (
	"context"
	"net/http"
	"os"

	"github.com/uptrace/opentelemetry-go-extra/otelzap"
	"gitlab.com/chanshare/manager/config"
	"gitlab.com/chanshare/manager/handlers"
	"gitlab.com/chanshare/manager/util"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	logger := buildLogger()
	ctx := context.Background()

	logger.Ctx(ctx).Info("Built logger")
	ctx = context.WithValue(ctx, util.Logger, logger)

	logger.Ctx(ctx).Info("Init config")
	cfg := config.InitConfig(ctx, "./.conf")
	logger.Ctx(ctx).Debug("Config loaded", zap.Any("cfg", cfg))

	tp, err := buildTracer(cfg.TracerProvider.URL)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			logger.Error("Error shutting down tracer provider: %v", zap.Error(err))
		}
	}()
	tracer := tp.Tracer(util.Manager)

	natsURL := cfg.Nats.Host + ":" + cfg.Nats.Port

	server, err := handlers.NewServer(cfg, tracer, logger, natsURL)
	if err != nil {
		panic(err)
	}

	addr := cfg.Service.Host + ":" + cfg.Service.Port
	logger.Ctx(ctx).Info("Listener started", zap.String("addr", addr))
	http.Handle("/", server.Router)
	logger.Ctx(ctx).Fatal("server exited", zap.Error(http.ListenAndServe(addr, nil)))
}

func buildLogger() *otelzap.Logger {
	highPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel
	})
	lowPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl < zapcore.ErrorLevel
	})

	consoleDebugging := zapcore.Lock(os.Stdout)
	consoleErrors := zapcore.Lock(os.Stderr)

	encoderConf := zap.NewDevelopmentEncoderConfig()
	encoderConf.EncodeLevel = zapcore.CapitalColorLevelEncoder
	encoderConf.TimeKey, encoderConf.EncodeTime = "timestamp", zapcore.ISO8601TimeEncoder
	encoder := zapcore.NewConsoleEncoder(encoderConf)

	cores := zapcore.NewTee(
		zapcore.NewCore(encoder, consoleErrors, highPriority),
		zapcore.NewCore(encoder, consoleDebugging, lowPriority),
	)
	return otelzap.New(zap.New(cores, zap.AddCaller()))
}

func buildTracer(url string) (*tracesdk.TracerProvider, error) {
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, err
	}

	tp := tracesdk.NewTracerProvider(
		tracesdk.WithBatcher(exp),
		tracesdk.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(util.Manager),
			attribute.String("env", "todo"),
		)),
	)

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	return tp, nil
}
