package config

import (
	"context"
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/chanshare/manager/util"
)

// Config The root config structure for the provisioner service
type Config struct {
	Service        ServiceCfg      `mapstructure:"service"`
	Loadbalancer   LoadbalancerCfg `mapstructure:"loadbalancer"`
	TracerProvider TracerProvider  `mapstructure:"tracer"`
	Nats           NatsCfg         `mapstructure:"nats"`
}

// ServiceCfg Configuration specific to the provisioner service
type ServiceCfg struct {
	Port string `mapstructure:"port"`
	Host string `mapstructure:"host"`
}

// LoadbalancerCfg config to tell the provisioner about the loadbalancer
type LoadbalancerCfg struct {
	Domain   string `mapstructure:"domain"`
	JoinSlug string `mapstructure:"join-slug"`
}

// PostgresCfg config to allow the service to access postgres
type NatsCfg struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}

type TracerProvider struct {
	URL string `mapstructure:"url"`
}

func readConfig(relPath string) Config {
	v := viper.New()
	v.SetConfigName("conf") // name of config file (without extension)
	v.SetConfigType("toml") // REQUIRED if the config file does not have the extension in the name
	// viper.AddConfigPath("/etc/prov/") // path to look for the config file in
	v.AddConfigPath(relPath) // call multiple times to add many search paths
	if err := v.ReadInConfig(); err != nil {
		panic(fmt.Errorf("failed to read in config %w\n", err))
	}
	var cfg Config
	if err := v.Unmarshal(&cfg); err != nil {
		panic(fmt.Errorf("fatal error config file %w\n", err))
	}
	return cfg
}

// InitConfig initialise the config from a config file
func InitConfig(ctx context.Context, relPath string) Config {
	logger, err := util.LoggerFromContext(ctx)
	if err != nil {
		panic(err)
	}

	logger.Info("Reading config")
	cfg := readConfig(relPath)

	return cfg
}
