package message

import (
	"encoding/binary"
	"errors"
)

var msgTooSmallErr = errors.New("Message too small")

const (
	Play  = 0x14
	Pause = 0x12
	Seek  = 0x11
	Join  = 0x41
	Leave = 0x42
	Next  = 0x21
	Prev  = 0x22
	Media = 0x30
	Kill  = 0xFF
)

var Mappings = map[int]string{
	Play:  "play",
	Pause: "pause",
	Seek:  "seek",
	Join:  "join",
	Leave: "leave",
	Next:  "next",
	Prev:  "prev",
	Media: "media",
	Kill:  "kill",
}

type Message struct {
	Action byte
	Param  interface{}
}

func (m Message) Encode() []byte {
	b := make([]byte, 0)
	b = append(b, m.Action)

	switch m.Param.(type) {
	case int:
		buf := make([]byte, 8)
		_ = binary.PutUvarint(buf, uint64(m.Param.(int)))
		b = append(b, buf...)
		break
	case string:
		s, _ := m.Param.(string)
		b = append(b, []byte(s)...)
		break
	default:
		break
	}
	return b
}

func Decode(bytes []byte) (Message, error) {
	var msg Message

	msg.Action = bytes[0]

	switch msg.Action {
	case Play, Pause, Seek:
		if len(bytes) <= 1 {
			return Message{
				Action: 0x00,
				Param:  nil,
			}, msgTooSmallErr
		}
		msg.Param = binary.LittleEndian.Uint64(bytes[1:])
		break
	case Join, Leave:
		if len(bytes) <= 1 {
			return Message{
				Action: 0x00,
				Param:  nil,
			}, msgTooSmallErr
		}
		msg.Param = string(bytes[1:])
		break
	default:
		break
	}

	return msg, nil
}
