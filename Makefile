UNAME := $(shell uname)

build:
	go build -v main.go

run: infra
	go run main.go

test:
	go test -v -coverprofile=coverage.out ./...
	go test -race ./...
	go tool cover -func=coverage.out
	go tool cover -html=coverage.out -o coverage.html
ifeq ($(UNAME), Linux)
		xdg-open coverage.html
endif
ifeq ($(UNAME), Darwin)
		open coverage.html
endif

test-ci: 
	go test -v -coverprofile=coverage.out ./...
	go test -race ./...
	go tool cover -func=coverage.out
	echo "total: $$(go tool cover -func=coverage.out | grep 'total:' | awk '{print $$3}' | sed -e 's/[%]//g')"

infra:
	cd ./infrastructure/ && $(MAKE) manager

stubs:
	cd ./infrastructure/ && $(MAKE) stubs
